import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.security.Key;

public class FX extends Application {

    private TextField textField;
    private int num1;
    private int num2;



    public static void main(String[] args){
        launch(args);
    }

    class MyHandle implements EventHandler<ActionEvent>{

        @Override
        public void handle(ActionEvent event){

            Button btn = (Button) event.getSource();

            String f = btn.getText();
            switch (f){
                case "C":
                    textField.setText("");
                    break;

                case"=":
                    //num2 = Integer.parseInt(textField.getText());
                    //textField.setText(String.valueOf(num1 + num2));
                    Expression expression = new ExpressionBuilder(textField.getText()).build();
                    textField.setText(String.valueOf(expression.evaluate()));
                    break;
                default:
                    textField.setText(textField.getText().concat(f));
            }
        }
    }


    class MyKeyHandler implements EventHandler<KeyEvent>{

        @Override
        public void handle (KeyEvent event){
            System.out.println("Key Event：" + event.getCode());
            String current = textField.getText();
            switch(event.getCode()){
                case DIGIT1:
                case NUMPAD1:
                    textField.setText(current.concat("1"));
                    break;
                case DIGIT2:
                case NUMPAD2:
                    textField.setText(current.concat("2"));
                    break;
                case DIGIT3:
                case NUMPAD3:
                    textField.setText(current.concat("3"));
                    break;
                case DIGIT4:
                case NUMPAD4:
                    textField.setText(current.concat("4"));
                    break;
                case DIGIT5:
                case NUMPAD5:
                    textField.setText(current.concat("5"));
                    break;
                case DIGIT6:
                case NUMPAD6:
                    textField.setText(current.concat("6"));
                    break;
                case DIGIT7:
                case NUMPAD7:
                    textField.setText(current.concat("7"));
                    break;
                case DIGIT8:
                case NUMPAD8:
                    textField.setText(current.concat("8"));
                    break;
                case DIGIT9:
                case NUMPAD9:
                    textField.setText(current.concat("9"));
                    break;
                case DIGIT0:
                case NUMPAD0:
                    textField.setText(current.concat("0"));
                    break;
                case BACK_SPACE:
                    textField.setText(current.substring(0,current.length()-1));
                    break;
                case ADD:
                    textField.setText(current.concat("+"));
                    break;
                case DIVIDE:
                    textField.setText(current.concat("-"));
                    break;
                case MULTIPLY:
                    textField.setText(current.concat("*"));
                    break;
                case SUBTRACT:
                    textField.setText(current.concat("/"));
                    break;
                case ENTER:
                    //textField.setText(current.concat("+"));
                    Expression expression = new ExpressionBuilder(textField.getText()).build();
                    textField.setText(String.valueOf(expression.evaluate()));
                    break;
                case ESCAPE:
                    textField.setText("");
                    break;
            }
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception{

        MyHandle myHandle = new MyHandle();
        MyKeyHandler myKeyHandler = new MyKeyHandler();
        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);

        gridPane.setOnKeyPressed(myKeyHandler);

        gridPane.setHgap(15);
        gridPane.setVgap(10);
        textField = new TextField();
        //textField.setEditable(false);
        //textField.setOnKeyPressed(myKeyHandler);

        gridPane.add(textField,0,0,4,1);
        Button btnOne = new Button("1");
        btnOne.setOnAction(myHandle);
        gridPane.add(btnOne,0,1);
        Button btnTwo = new Button("2");
        btnTwo.setOnAction(myHandle);
        gridPane.add(btnTwo,1,1);
        Button btnThree = new Button("3");
        btnThree.setOnAction(myHandle);
        gridPane.add(btnThree,2,1);
        Button btnClear = new Button("C");
        btnClear.setOnAction(myHandle);
        gridPane.add(btnClear,3,1);
        Button btnFour = new Button("4");
        btnFour.setOnAction(myHandle);
        gridPane.add(btnFour,0,2);
        Button btnFive = new Button("5");
        btnFive.setOnAction(myHandle);
        gridPane.add(btnFive,1,2);
        Button btnSix = new Button("6");
        btnSix.setOnAction(myHandle);
        gridPane.add(btnSix,2,2);
        Button btnResult = new Button("=");
        btnResult.setOnAction(myHandle);
        gridPane.add(btnResult,3,2);
        Button btnSeven = new Button("7");
        btnSeven.setOnAction(myHandle);
        gridPane.add(btnSeven,0,3);
        Button btnEight = new Button("8");
        btnEight.setOnAction(myHandle);
        gridPane.add(btnEight,1,3);
        Button btnNine = new Button("9");
        btnNine.setOnAction(myHandle);
        gridPane.add(btnNine,2,3);
        gridPane.add(new Button("/"),3,3);
        Button btnZero = new Button("0");
        btnZero.setOnAction(myHandle);
        gridPane.add(btnZero,0,4);
        Button btnPlus = new Button("+");
        btnPlus.setOnAction(myHandle);
        gridPane.add(btnPlus,1,4);
        gridPane.add(new Button("-"),2,4);


        gridPane.add(new Button("*"),3,4);

        Scene scene = new Scene(gridPane,600,400);
        //textField.setFocusTraversable(false);
        textField.setText("1+1");
        primaryStage.setTitle("My little Calculator");
        primaryStage.setScene(scene);
        setUserAgentStylesheet(STYLESHEET_CASPIAN);
        primaryStage.show();
    }}
